﻿using System;
using DansVan.RollerCommand;

namespace DansConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            var d1 = new Dice()
            {
                Amount = 1,
                Sides = 6,
                Bonuses = 0
            };
            var d2 = new Dice()
            {
                Amount = 1,
                Sides = 6,
                Bonuses = 0
            };

            var calc = new DiceProbabilityCalculator(d1, d2, true);

            Console.WriteLine(calc.printChartResults());
            Console.WriteLine(calc.printPercentageResults());
            Console.ReadLine();
        }
    }
}
