﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DansVan.WuTangCommand
{
    class WuTangNameCommand
    {
        [Command("wutang"), Description("Get yo wutang name, ")]
        public async Task WuTang(CommandContext ctx, string s)
        {
            var options = new ChromeOptions();
            options.AddArgument("--headless");
            using (var driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), options))
            {
                driver.Navigate().GoToUrl(@"http://www.mess.be/inickgenwuname.php");
                var nameBox = driver.FindElement(By.Name("realname"));
                nameBox.SendKeys(s);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(1));
                var clickableElement = wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("Submit")));
                clickableElement.Click();

                var newName = driver.FindElementByXPath(@"//center//font//b//font");

                await ctx.RespondAsync("Wu-tang clan ain't nuthin to fuck with " + newName.Text + "!");
            }
        }
    }
}
