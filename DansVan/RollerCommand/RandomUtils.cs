﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DansVan.RollerCommand
{
    class RandomUtils
    {
        //Function to get a random number 
        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();

        public static int RandomNumber(int min, int max)
        {
            lock (syncLock)
            { // synchronize
                return random.Next(min, max);
            }
        }

    }
}
