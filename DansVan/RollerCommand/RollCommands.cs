﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DansVan.RollerCommand
{
    class RollCommands
    {

        [Command("roll"), Description("Roll sillco style dice")]
        public async Task Roll(CommandContext ctx, string s, int i = 0, string asst = "")
        {
            Roller roller;
            try
            {
                roller = new Roller(s, i, asst);

                if (roller.dice.Amount > 50)
                {
                    await ctx.RespondAsync("Dice rolls too high, can only roll up to 50");
                }
                else if (roller.dice.Sides != 6)
                {
                    await ctx.RespondAsync("Sillco is designed to only roll d6");
                }
                else
                {
                    await ctx.RespondAsync("Rolled: " + roller.Roll());
                }
            }
            catch (DiceException e)
            {
                await ctx.RespondAsync(e.Message);
            }
        }

        [Command("problist"), Description("Get percentage for the difference between two rolls ")]
        public async Task ProbabilityList(CommandContext ctx, string s1, int bonus1, string s2, int bonus2)
        {
            DiceProbabilityCalculator calc;
            try
            {
                Dice d1 = RollerUtils.parseDice(s1);
                Dice d2 = RollerUtils.parseDice(s2);
                calc = new DiceProbabilityCalculator(d1, d2, true);
                await ctx.RespondAsync(calc.printPercentageResults());
            }
            catch (Exception e)
            {
                await ctx.RespondAsync(e.Message);
            }
        }

        [Command("probchart"), Description("Get chart for the difference between two rolls")]
        public async Task ProbabilityChart(CommandContext ctx, string s1, int bonus1, string s2, int bonus2)
        {
            DiceProbabilityCalculator calc;
            try
            {
                Dice d1 = RollerUtils.parseDice(s1);
                Dice d2 = RollerUtils.parseDice(s2);
                calc = new DiceProbabilityCalculator(d1, d2, true);
                await ctx.RespondAsync(calc.printChartResults());
            }
            catch (Exception e)
            {
                await ctx.RespondAsync(e.Message);
            }
        }
    }

}
