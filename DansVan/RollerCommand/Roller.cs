﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace DansVan.RollerCommand
{
    public class Roller
    {
        public Dice dice { get; private set; }
        private bool IsAssist { get; set; }         // adds rules for assist
        private int Bonus { get; set; }
        private List<string> RollList { get; set; }
        private ConcurrentDictionary<int, int> Rolls { get; set; }

        public Roller(string input, int bonus, string assist)
        {
            Bonus = bonus;
            Rolls = new ConcurrentDictionary<int, int>();
            RollList = new List<string>();
            Dice dice = RollerUtils.parseDice(input);

            IsAssist = false;
            if (!string.IsNullOrEmpty(assist))
            {
                IsAssist = true;
            }
        }

        public string Roll()
        {
            for (int i = 0; i < dice.Amount; i++)
            {
                int roll = RandomUtils.RandomNumber(0, dice.Sides) + 1;
                Print(roll);
                Rolls.AddOrUpdate(roll, 1, (key, count) => count + 1);
                RollList.Add(roll.ToString());
            }
            return BuildOutput(CalculateRoll());
        }

        string BuildOutput(int calculatedRoll)
        {
            string output = "";
            output += "(";
            for (int i = 0; i < RollList.Count; i++)
            {
                if (i < RollList.Count - 1)
                {
                    output += RollList[i] + ",";
                }
                else
                {
                    output += RollList[i];
                }
            }
            if (Bonus > 0)
            {
                output += ") + " + Math.Abs(Bonus);
            }
            else if (Bonus < 0)
            {
                output += ") - " + Math.Abs(Bonus);
            }
            else
            {
                output += ")";
            }
            output += "\nFinal roll: " + calculatedRoll;
            return output;
        }

        int CalculateRoll()
        {
            int maxRoll = Rolls.Keys.Max();

            // gives the roll if it was a 6
            if (maxRoll == dice.Sides)
            {
                return maxRoll + (Rolls[maxRoll] - 1) + Bonus;
            }

            // gives the roll if it had assist.
            if (IsAssist)
            {
                /*
                int assist = 0;
                for (int i = 2; i <= Rolls[maxRoll]; i++)
                {
                    if (IsPowerOfTwo(i))
                    {
                        Console.WriteLine(i);
                        assist++;
                    }
                }
                return maxRoll + assist + Bonus;
                */
            }

            // gives the base roll calculation.
            int finalRoll = maxRoll + Bonus;
            if (finalRoll < 1)
            {
                return 1;
            }
            return finalRoll;
        }

        public static void Print(int i)
        {
            Console.WriteLine(i);
        }

        public static bool IsPowerOfTwo(int x)
        {
            return (x != 0) && ((x & (x - 1)) == 0);
        }
    }

}
