﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DansVan.RollerCommand
{
    public class Dice
    {
        public int Sides { get; set; }
        public int Amount { get; set; }
        public int Bonuses { get; set; }

        /// <summary>
        /// Calculates the maximum roll that these dice can have.
        /// </summary>
        /// <returns>Highest roll possible</returns>
        public int MaxRoll()
        {
            return (Sides * Amount) + Bonuses;
        }

        /// <summary>
        /// Calculates the lowest roll that the dice can roll
        /// </summary>
        /// <returns>Lowest roll possible</returns>
        public int MinRoll()
        {
            return Amount + Bonuses;
        }

        /// <summary>
        /// Range of rolls that are possible
        /// </summary>
        /// <returns></returns>
        public int Range()
        {
            return MaxRoll() - (MinRoll() - 1);
        }
    }
}
