﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace DansVan.RollerCommand
{
    public class DiceProbabilityCalculator
    {
        public ConcurrentDictionary<int, int> Rolls { get; private set; }
        public int[,] DiceArray { get; private set; }

        private Dice d1;
        private Dice d2;

        private int totalRolls;

        public DiceProbabilityCalculator(Dice d1, Dice d2, bool onlyPositive = false)
        {
            this.d1 = d1;
            this.d2 = d2;
            Rolls = new ConcurrentDictionary<int, int>();
            totalRolls = d1.Range() * d2.Range();
            DiceArray = new int[this.d1.Range(), this.d2.Range()];

            CalculateProbability(onlyPositive);
        }

        void CalculateProbability(bool onlyPositive = false)
        {
            for (int i = 0; i <= DiceArray.GetLength(0) - 1; i++)
            {
                for (int j = 0; j <= DiceArray.GetLength(1) - 1; j++)
                {
                    int attackerRoll = this.d1.MinRoll() + i;
                    int defenderRoll = this.d2.MinRoll() + j;
                    var outcome = attackerRoll - defenderRoll;
                    if (onlyPositive)
                    {
                        outcome = outcome >= 0 ? outcome : 0;
                    }
                    Rolls.AddOrUpdate(outcome, 1, (key, count) => count + 1);
                    DiceArray[i, j] = outcome;

                }
            }
        }

        public string printChartResults()
        {
            StringBuilder sb = new StringBuilder();
            int rowLength = DiceArray.GetLength(0);
            int colLength = DiceArray.GetLength(1);

            for (int i = 0; i < rowLength; i++)
            {
                for (int j = 0; j < colLength; j++)
                {
                    sb.AppendFormat("{0} ", DiceArray[i, j]);
                }
                sb.Append(System.Environment.NewLine + System.Environment.NewLine);
            }
            return sb.ToString();
        }

        public string printPercentageResults()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var nums in Rolls)
            {
                sb.AppendFormat("{0}: {1}", nums.Key, (double)nums.Value / (double)totalRolls);
                sb.Append(System.Environment.NewLine);
            }
            return sb.ToString();
        }
    }
}
