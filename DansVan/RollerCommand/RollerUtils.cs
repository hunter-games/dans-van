﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DansVan.RollerCommand
{
    class RollerUtils
    {
        public static Dice parseDice(string diceString)
        {
            var dice = new Dice();
            string[] formatted = diceString.Split("d");
            dice.Amount = parseAmount(formatted[0]);
            dice.Sides = parseSides(formatted[1]);
            return dice;
        }

        static int parseAmount(string amount)
        {
            int diceAmount;
            if (int.TryParse(amount, out diceAmount))
            {
                return diceAmount;
            }
            else
            {
                throw new DiceException("Could not parse dice thrown number");
            }
        }

        static int parseSides(string sides)
        {
            int diceSides;
            if (int.TryParse(sides, out diceSides))
            {
                return diceSides;
            }
            else
            {
                throw new DiceException("Could not parse dice size number");
            }
        }
    }
}
