﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DansVan.RollerCommand
{
    public class DiceException : Exception
    {
        public DiceException() { }

        public DiceException(string message) : base(message) { }

        public DiceException(string message, Exception inner) : base(message, inner) { }
    }
}
