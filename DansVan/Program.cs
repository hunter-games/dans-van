﻿using DansVan.RollerCommand;
using System;

namespace DansVan
{
    class Program
    {
        static void Main(string[] args)
        {
            var dansVan = new DansVan();

            dansVan.MainAsync(args)
                .GetAwaiter()
                .GetResult();
        }
    }
}
